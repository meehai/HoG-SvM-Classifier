#include "DatasetMeta.hpp"

DatasetMeta::DatasetMeta(const std::string &name, const std::vector<std::string> &classes,
	const std::vector<std::string> &paths) : name(name), classes(classes), paths(paths) { }

void DatasetMeta::addClass(const std::string &className, const std::string &classPath) {
	this->classes.push_back(className);
	this->paths.push_back(classPath);
}